import re
import traceback
from time import sleep

import glob
import requests
from peewee import Model, IntegerField, CharField, ForeignKeyField, DateTimeField, FloatField, IntegrityError

from database import db
from bs4 import BeautifulSoup as bs

from utils import get_filename_from_cd


class Status(Model):
    step = CharField(null=True)

    class Meta:
        database = db


class Category(Model):
    name = CharField(null=True)
    url = CharField(null=True)
    parent = ForeignKeyField('self', related_name='subcategories', null=True)
    image_name = CharField(null=True)

    class Meta:
        database = db

    def add_main_image(self, url):
        r = requests.get(url, allow_redirects=True, timeout=120)
        file_name = url.split('/')[-1]
        img_file = open(f'tmp/category_i/{file_name}', 'wb')
        img_file.write(r.content)
        img_file.close()
        self.image_name = file_name
        self.save()


    def parse_products(self):
        have_next_page = True
        url = self.url + 'count_200/'
        while have_next_page:
            response = requests.post(url, data={'nalich': 1}, timeout=120)
            while response.status_code == 502:
                print('Сайт вернул 502 ошибку\n\n')
                response = requests.post(url, data={'nalich': 1}, timeout=120)
                sleep(5)
            soup = bs(response.content, 'lxml')
            divs = soup.select('div.obolochka')
            for div in divs:
                product_url = div.select('.detshow > a')[0].get('href')
                product_name = div.select('.detshow > a > img')[0].get('alt')
                product = Product.filter(Product.name == product_name, Product.url == product_url).first()
                if product:
                    product.category = self
                    product.save()
                else:
                    try:
                        Product(category=self, url=product_url, name=product_name).save()
                    except IntegrityError:
                        print('Дубль')
            try:
                have_next_page = True if soup.select('div.navigation a')[-1].text == 'Далее' else False
            except IndexError:
                have_next_page = False

            if have_next_page:
                url = soup.select('div.navigation a')[-1].get('href')


class Product(Model):
    category = ForeignKeyField(Category, related_name='products')
    name = CharField()
    url = CharField()

    coast = FloatField(null=True)
    article = IntegerField(null=True)

    class Meta:
        database = db
        indexes = (
            (('name', 'url'), True),
        )

    def is_parse(self):
        pass

    def get_image_list(self):
        mylist = [f for f in glob.glob(f"tmp/i/{self.article}_*.*")]
        edit_list = [re.findall(r'tmp\/i\\(\d+_\d+.\S+)', f)[0] for f in mylist]
        return sorted(edit_list)

    def get_file_list(self):
        mylist = [f for f in glob.glob(f"tmp/f/{self.article}_*.*")]
        edit_list = [re.findall(r'tmp\/f\\(\d+_\d+.\S+)', f)[0] for f in mylist]
        return sorted(edit_list)

    def get_image_count(self):
        mylist = [f for f in glob.glob(f"tmp/i/{self.article}_*.*")]
        return len(mylist)

    def get_file_count(self):
        mylist = [f for f in glob.glob(f"tmp/f/{self.article}_*.*")]
        return len(mylist)

    def add_image(self, url):
        r = requests.get(url, allow_redirects=True, timeout=120)
        file_type = get_filename_from_cd(r.headers.get('content-disposition')).split('.')[-1]
        file_name = f'{self.article}_{self.get_image_count() + 1}.{file_type}'
        img_file = open(f'tmp/i/{file_name}', 'wb')
        img_file.write(r.content)
        img_file.close()

    def add_file(self, url):
        r = requests.get(url, allow_redirects=True, timeout=120)
        file_type = get_filename_from_cd(r.headers.get('content-disposition')).split('.')[-1].replace('"','')
        file_name = f'{self.article}_{self.get_file_count() + 1}.{file_type}'
        file = open(f'tmp/f/{file_name}', 'wb')
        file.write(r.content)
        file.close()

    def parse_product_info(self):
        try:
            response = requests.get(self.url, timeout=120)
            if response.status_code == 200:
                soup = bs(response.content, 'lxml')
                self.article = int(soup.select('i:contains("Артикул:") + b')[0].text)
                try:
                    self.coast = float(soup.select('input[name^="prise"]')[0].get('value'))
                except IndexError:
                    self.coast = 0
                # print('Парсинг продукта', self.name)
                # print('артикль:', self.article)
                # print('цена:', self.coast)
                div_params = soup.select('table.param tr')
                table = ''
                for param in div_params:
                    key = param.select('th')[0].text
                    value = param.select('td')[0].text
                    table += f'<tr>\n\t\t<th>\n\t\t\t{key}\n\t\t</th>\n\t\t<td>\n\t\t\t{value}\n\t\t</td>\n\t</tr>\n\t'
                if table:
                    table = f'<table>\n\t{table}\n</table>'
                    with open(f'tmp/p/{self.article}.html', 'w', encoding='utf-8') as file:
                        file.write(table)
                div_images = soup.select('a[href^="http://www.zip-2002.ru/jpg/image.php?id="]')
                for image in div_images:
                    self.add_image(image.get('href'))
                div_files = soup.select('a[href^="http://www.zip-2002.ru/jpg/file.php?id="]')
                for file in div_files:
                    self.add_file(file.get('href'))
                # print('кол-во картинок:', self.get_image_count())
                # print('кол-во файлов:', self.get_file_count())
                self.save()
            elif response.status_code == 404:
                raise Exception('Невозможно найти страницу содержимого товара. Ошибка 404.')
            else:
                raise Exception(f'Ошибка при получении содержимого товара. Статус запроса: {response.status_code}')
        except Exception as e:
            print(e)
            print(traceback.print_exc())

