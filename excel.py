# -*- coding: utf-8 -*-
import glob
import re
from io import BytesIO

import xlsxwriter

import settings

def max_items(files_list):
    max = 0
    for item in files_list:
        number = re.findall(r'\d+_(\d+).\S+', item)
        if max < int(number[0]):
            max = int(number[0])
    return max


def get_max_images_count():
    files_list = [f for f in glob.glob("tmp/i/*_*.*")]
    return max_items(files_list)


def get_max_files_count():
    files_list = [f for f in glob.glob("tmp/f/*_*.*")]
    return max_items(files_list)

def generate_excel():
    workbook = xlsxwriter.Workbook('tmp/export.xlsx')
    worksheet = workbook.add_worksheet()

    worksheet.set_column('A:A', 7)
    worksheet.set_column('B:B', 60)
    worksheet.set_column('C:C', 6)
    worksheet.set_column('D:D', 4)
    worksheet.set_column('E:E', 5)
    return workbook, worksheet


def set_table_header(workbook, worksheet, parsing_date=None):
    bold_center = workbook.add_format({'bold': True})
    bold_center.set_align('center')
    bold_center.set_border(style=1)
    bold_center.set_font_size(9)
    bold_center.set_font_name('Arial')

    main_category_style = workbook.add_format()
    main_category_style.set_color('brown')
    main_category_style.set_font_size(16)
    main_category_style.set_bold()
    main_category_style.set_italic()
    main_category_style.set_font_name('Arial')

    date = ''
    if parsing_date:
        date = f': {parsing_date}'
    worksheet.write('A1', f'Складская шахматка на дату{date}',
                    main_category_style)
    worksheet.write('A2', 'Код', bold_center)
    worksheet.write('B2', 'Товар', bold_center)
    worksheet.write('C2', 'розн', bold_center)
    worksheet.write('C3', 'цена', bold_center)
    worksheet.write('D2', 'f4', bold_center)
    worksheet.write('E2', 'К/Т', bold_center)

    alphabet = ['F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

    worksheet.image_start_col = alphabet[0]
    for i in range(get_max_images_count()):
        worksheet.set_column(f'{alphabet[0]}:{alphabet[0]}', 35)
        worksheet.write_url(f'{alphabet[0]}2', f'i{i+1}', bold_center)
        alphabet.pop(0)
    worksheet.file_start_col = alphabet[0]
    for i in range(get_max_files_count()):
        worksheet.set_column(f'{alphabet[0]}:{alphabet[0]}', 35)
        worksheet.write_url(f'{alphabet[0]}2', f'f{i+1}', bold_center)
        alphabet.pop(0)

    worksheet.parameter_start_col = alphabet[0]
    worksheet.set_column(f'{alphabet[0]}:{alphabet[0]}', 35)
    worksheet.write_url(f'{alphabet[0]}2', 'p1', bold_center)

    worksheet.write('A3', '', bold_center)
    worksheet.write('B3', '', bold_center)
    worksheet.write('D3', '', bold_center)
    worksheet.write('E3', '', bold_center)

    return workbook, worksheet



def set_table_category_1(workbook, worksheet, index, category_info):
    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('black')
    main_category_style.set_color('gray')
    main_category_style.set_font_size(12)
    main_category_style.set_font_name('Arial')
    main_category_style.set_align('center')

    worksheet.write(f'A{index}', '', main_category_style)
    worksheet.write(f'C{index}', '', main_category_style)
    worksheet.write(f'E{index}', 'К1', main_category_style)

    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('black')
    main_category_style.set_color('gray')
    main_category_style.set_font_size(12)
    main_category_style.set_font_name('Arial')
    if category_info.image_name:
        worksheet.write(f'D{index}', f'https://{settings.HOST}/media/category_i/{category_info.image_name}', main_category_style)
    else:
        worksheet.write(f'D{index}', f'', main_category_style)

    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('black')
    main_category_style.set_color('white')
    main_category_style.set_bold()
    main_category_style.set_font_size(12)
    main_category_style.set_font_name('Arial')

    worksheet.write(f'B{index}', category_info.name, main_category_style)


    return workbook, worksheet


def set_table_category_2(workbook, worksheet, index, category_info):
    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('gray')
    main_category_style.set_color('black')
    main_category_style.set_font_size(10)
    main_category_style.set_font_name('Arial')
    main_category_style.set_align('center')
    main_category_style.set_border()

    worksheet.write(f'A{index}', '', main_category_style)
    worksheet.write(f'C{index}', '', main_category_style)
    worksheet.write(f'E{index}', 'К2', main_category_style)

    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('gray')
    main_category_style.set_color('black')
    main_category_style.set_font_size(10)
    main_category_style.set_font_name('Arial')
    main_category_style.set_border()
    if category_info.image_name:
        worksheet.write(f'D{index}', f'https://{settings.HOST}/media/category_i/{category_info.image_name}', main_category_style)
    else:
        worksheet.write(f'D{index}', f'', main_category_style)

    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('gray')
    main_category_style.set_color('black')
    main_category_style.set_bold()
    main_category_style.set_font_size(10)
    main_category_style.set_font_name('Arial')
    main_category_style.set_border()

    worksheet.write(f'B{index}', category_info.name, main_category_style)


    return workbook, worksheet


def set_table_category_3(workbook, worksheet, index, category_info):
    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('gray')
    main_category_style.set_color('black')
    main_category_style.set_font_size(8)
    main_category_style.set_font_name('Arial')
    main_category_style.set_align('center')
    main_category_style.set_border()

    worksheet.write(f'A{index}', '', main_category_style)
    worksheet.write(f'C{index}', '', main_category_style)
    worksheet.write(f'E{index}', 'К3', main_category_style)

    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('gray')
    main_category_style.set_color('black')
    main_category_style.set_font_size(8)
    main_category_style.set_font_name('Arial')
    main_category_style.set_border()
    if category_info.image_name:
        worksheet.write(f'D{index}', f'https://{settings.HOST}/media/category_i/{category_info.image_name}', main_category_style)
    else:
        worksheet.write(f'D{index}', f'', main_category_style)

    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('gray')
    main_category_style.set_color('black')
    main_category_style.set_bold()
    main_category_style.set_font_size(10)
    main_category_style.set_font_name('Arial')
    main_category_style.set_border()

    worksheet.write(f'B{index}', category_info.name, main_category_style)


    return workbook, worksheet


def set_table_product(workbook, worksheet, index, product_info, codes_map):
    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('yellow')
    main_category_style.set_color('black')
    main_category_style.set_font_size(8)
    main_category_style.set_font_name('Arial')
    main_category_style.set_align('center')
    main_category_style.set_border()

    worksheet.write(f'A{index}', product_info.article, main_category_style)

    main_category_style = workbook.add_format()
    main_category_style.set_bg_color('white')
    main_category_style.set_color('black')
    main_category_style.set_font_size(8)
    main_category_style.set_font_name('Arial')
    main_category_style.set_align('center')
    main_category_style.set_border()

    worksheet.write(f'B{index}', product_info.name, main_category_style)

    main_category_style = workbook.add_format()
    if product_info.coast > 0:
        main_category_style.set_bg_color('#00ff13')
    else:
        main_category_style.set_bg_color('red')
    main_category_style.set_color('black')
    main_category_style.set_font_size(8)
    main_category_style.set_font_name('Arial')
    main_category_style.set_align('center')
    main_category_style.set_border()

    worksheet.write(f'C{index}', product_info.coast, main_category_style)


    main_category_style = workbook.add_format()
    main_category_style.set_color('black')
    main_category_style.set_font_size(8)
    main_category_style.set_font_name('Arial')
    main_category_style.set_align('center')
    main_category_style.set_border()
    worksheet.write(f'E{index}', 'Т', main_category_style)

    main_category_style = workbook.add_format()
    main_category_style.set_color('black')
    main_category_style.set_font_size(8)
    main_category_style.set_font_name('Arial')
    main_category_style.set_border()
    if product_info.article in codes_map:
        worksheet.write(f'D{index}', f'https://{settings.HOST}/media/main_i/{codes_map[product_info.article]}.jpg', main_category_style)
    else:
        worksheet.write(f'D{index}', f'', main_category_style)
    main_category_style = workbook.add_format()
    main_category_style.set_color('black')
    main_category_style.set_font_size(8)
    main_category_style.set_font_name('Arial')
    main_category_style.set_border()


    alphabet = ['F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

    alphabet_index = alphabet.index(worksheet.image_start_col)
    for i, value in enumerate(product_info.get_image_list()):
        worksheet.write(f'{alphabet[alphabet_index]}{index}', f'https://{settings.HOST}/media/i/{value}', main_category_style)
        alphabet_index += 1

    alphabet_index = alphabet.index(worksheet.file_start_col)
    for i, value in enumerate(product_info.get_file_list()):
        worksheet.write(f'{alphabet[alphabet_index]}{index}', f'https://{settings.HOST}/media/f/{value}', main_category_style)
        alphabet_index += 1


    worksheet.write(f'{alphabet[alphabet.index(worksheet.parameter_start_col)]}{index}', f'https://{settings.HOST}/media/p/{product_info.article}_1.html', main_category_style)


    return workbook, worksheet







