import os
import re
import traceback
from datetime import datetime
from time import sleep
import shutil
import signal

import requests
from clint.textui import progress

from database import db
from get_code_map_by_article import get_code_map

from models import Category, Product, Status
from bs4 import BeautifulSoup as bs

from excel import generate_excel, set_table_header, set_table_category_1, set_table_category_2, \
    set_table_product, set_table_category_3

excel_levels = {
    1: set_table_category_1,
    2: set_table_category_2,
    3: set_table_category_3,
    4: set_table_category_3,
    5: set_table_category_3,
}

FOLDERS_TO_CHECK = [
    'tmp',
    'tmp/f',
    'tmp/i',
    'tmp/p',
    'tmp/category_i',
    'finish',
]

header = """\n
                          _ _      __ __   __  
                         | (_)    /_ /_ | / /  
  __ _ _ __ ___   ___  __| |_  __ _| || |/ /_  
 / _` | '_ ` _ \ / _ \/ _` | |/ _` | || | '_ \ 
| (_| | | | | | |  __/ (_| | | (_| | || | (_) |
 \__,_|_| |_| |_|\___|\__,_|_|\__,_|_||_|\___/ 
                           parsing zip-2002.ru
               """


def check_or_create_folder(folder):
    if not os.path.exists(folder):
        os.mkdir(folder)


def check_folders():
    try:
        for folder in FOLDERS_TO_CHECK:
            check_or_create_folder(folder)
        return True
    except Exception as e:
        print(f'Ошибка во время выполнения проверки файлов. Текст ошибки: {e}')
    return False


def create_table():
    Category.create_table()
    Product.create_table()
    Status.create_table()


def parsing_category():
    if Category.select():
        print('Очиста старых категорий')
    for category in Category.select():
        category.delete()
    response = requests.get('http://www.zip-2002.ru/', timeout=120)
    soup = bs(response.content, 'lxml')
    divs = soup.select('ul.gnm')
    for div in divs:
        name = div.select('li > a')[0].text
        url = div.select('li > a')[0].get('href')
        main_category = Category(name=name, url=url)
        main_category.save()
        for li in div.select('ul:not(.gnm) > li'):
            name = li.select('a')[0].text
            url = li.select('a')[0].get('href')
            Category(name=name, url=url, parent=main_category).save()


def parsing_category_image():
    for category in Category.select():
        response = requests.get(category.url, timeout=120)
        soup = bs(response.content, 'lxml')
        divs = soup.select('.wat_t')
        for div in divs:
            image_url = div.select('.w_t')[0].get('src')
            category_url = div.select('a')[0].get('href')
            category = Category.select().where(Category.url == category_url)[0]
            if category:
                category.add_main_image(image_url)


def get_status_or_create():
    status = Status.filter(Status.id == 1).first()
    if not status:
        status = Status(step='create')
        status.save()
    return status


def parsing_products():
    for category in progress.bar(Category.select()):
        category.parse_products()


def parsing_products_info():
    try_count = 3
    while Product.select().where(Product.coast == None, Product.article == None) and try_count:
        for product in progress.bar(Product.select().where(Product.coast == None, Product.article == None)):
            product.parse_product_info()
        try_count -= 1
    if try_count:
        return True
    else:
        return False


def get_folder_name():
    date = datetime.now()
    folder_name = f'{date.day}_{date.month}_{date.year}_{date.hour}_{date.minute}'
    return folder_name


def get_time():
    date = datetime.now()
    folder_name = f'{date.day}.{date.month}.{date.year} {date.hour}:{date.minute}'
    return folder_name


def finishing_file():
    folder_name = get_folder_name()
    os.mkdir(f'finish/{folder_name}')
    shutil.move('tmp/i', f'./finish/{folder_name}/i')
    shutil.move('tmp/f', f'./finish/{folder_name}/f')
    shutil.move('tmp/p', f'./finish/{folder_name}/p')
    shutil.move('tmp/category_i', f'./finish/{folder_name}/category_i')
    shutil.move('tmp/export.xlsx', f'./finish/{folder_name}/export.xlsx')
    db.close()
    os.remove('parsing.db')


def clear_parsing():
    try:
        db.close()
        os.remove('parsing.db')
        shutil.rmtree('tmp')
    except:
        pass
    print('Файлы успешно очищены')

def excel_recursion_category(workbook, worksheet, this_category, index, level=1):
    workbook, worksheet = excel_levels[level](workbook, worksheet, index, this_category)
    codes_map = get_code_map()
    index += 1
    for product in this_category.products:
        workbook, worksheet = set_table_product(workbook, worksheet, index, product, codes_map)
        index += 1
    for category in this_category.subcategories:
        index = excel_recursion_category(workbook, worksheet, category, index, level+1)
    return index


def create_excel_file():
    index = 4   # первая строка данных таблицы
    workbook, worksheet = generate_excel()
    workbook, worksheet = set_table_header(workbook, worksheet, get_time())
    for main_category in Category.filter(Category.parent == None):
        index = excel_recursion_category(workbook, worksheet, main_category, index)
    workbook.close()




def parsing():
    create_table()
    check_folders()
    status = get_status_or_create()
    if status.step == 'create':
        print('Сбор категорий')
        parsing_category()
        status.step = 'category_image'
        status.save()
    if status.step == 'category_image':
        print('Сбор картинок категорий')
        parsing_category_image()
        status.step = 'category'
        status.save()
    if status.step == 'category':
        print('Сбор ссылок на продукты по категориям')
        parsing_products()
        status.step = 'products_urls'
        status.save()
    if status.step == 'products_urls':
        print('Сбор информации по продуктам')
        if parsing_products_info():
            status.step = 'done_for_excel'
            status.save()
        else:
            print('Превышен лимит ошибок при получении информации о продуктах. '
                  'Через 30 секунд автоматически перезапустится парсинг.')
            sleep(30)
            parsing()
    if status.step == 'done_for_excel':
        print('Формирование Excel файла')
        create_excel_file()
        status.step = 'finishing'
        status.save()
    if status.step == 'finishing':
        print('Формирования пакета готовых данных')
        finishing_file()
        print('Все спаршено!')


def main():
    print(header)
    while True:
        try:
            clear = lambda: os.system('cls')
            # clear()
            menu = (
                '1. Запустить парсинг\n'
                '2. Очистить файлы незаконченного парсинга\n'
                '99. Выход\n'
            )
            print(menu)
            action = int(input('Действие [1-99]: '))
            if action == 1:
                parsing()
            if action == 2:
                clear_parsing()
            elif action == 99:
                exit(0)
            else:
                pass
        except Exception as e:
            print(e)
            print(traceback.print_exc())


if __name__ == '__main__':
    main()
