import requests
import zipfile
import io

DATABASE_TXT_URL: str = 'http://www.zip-2002.ru/vip/klassifikator.zip'


def get_code_map():
    product_map = {}

    r = requests.get(DATABASE_TXT_URL, timeout=120)
    z = zipfile.ZipFile(io.BytesIO(r.content))
    with z.open(z.filelist[0], 'r') as text:
        for line in text.readlines()[1:]:
            line_array = line.decode('ISO-8859-1').split('\t')
            if line_array[0] and line_array[3]:
                product_map.update(
                    {int(line_array[0]): int(line_array[3])}
                )

    return product_map



